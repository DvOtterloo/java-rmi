#!/bin/bash

cd /Applications/xampp/htdocs/classes/helloworld

:: Start java met het juiste classpath
java -cp HelloIF.jar;HelloServer.jar;./dependencies/log4j-1.2.17.jar example.hello.Server localhost

:: Wanneer je securityproblemen wilt oplossen, voeg dan onderstaande optie aan het command toe.
:: Hiermee krijg je inzicht in alle security instellingen.
::
:: 		-Djava.security.debug=access,failure
